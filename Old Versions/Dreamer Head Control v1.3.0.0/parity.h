// parity.h
// Contains headers required for use of parity functions.

// This file is part of Dreamer Head v1.0.1.2.
// Travis Llado, travis@travisllado.com
// Last modified 2017.02.27

////////////////////////////////////////////////////////////////////////////////
// parity()
// A simple function that calculates the parity bit required for a given number.

uint32_t parity(uint32_t num);

////////////////////////////////////////////////////////////////////////////////
// End of file
