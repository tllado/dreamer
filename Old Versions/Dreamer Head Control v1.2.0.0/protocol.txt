Each command is a 32-bit word transmitted over UART at 115200 baud.
- First seven bits (31-25) identify the register to be written to.
- Next twenty four bits (24-01) are the value to be written.
- Last bit (00) is even parity for the previous 31 bits (31-01).
- Bit 31 is MSB, Bit 00 is LSB

Registers:
00  J00 position
01  J01 position
... ...
10  J10 position
11  J11 position
    Desired encoder positions for each individual joint. If desired position is 
    greater than joint maximum or less than joint minimum, joint will move to 
    respective limit. Joints will not move until "update position" register is
    written to.

13  move length (num cycles)
    Number of control loop cycles from start to end of next movement. If no
    value is specified or value specified is below minimum allowable, then
    previous or default value will be used.

15  update position
    Writing any value to this register starts movement toward the current
    ordered positions.

17  run program #
    Immediately switch to specified program number

19  light color
    Immediately change ear lights to specified color value

Examples:

    register|         value          |parity
------------+------------------------+------
Ex.1 0000011 000000000010110011101100 0
Ex.2 0010001 000000000000000000000010 1
Ex.3 0010011 001100100011110001101000 1

Example 1: Joint 3, desired position = 11500
Example 2: Run program 2
Example 3: Ear lights, desired color = 0x323C68
